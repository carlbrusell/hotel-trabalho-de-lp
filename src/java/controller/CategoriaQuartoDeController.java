/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.ClienteDAO;
import java.io.IOException;
import java.sql.Date;
import java.util.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.CategoriaQuartoDAO;
import model.Cliente.*;
import model.CategoriaQuarto;
import dao.HotelDAO;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author carlbr
 */
@WebServlet(name = "CategoriaQuartoServlet", urlPatterns = {"/categoriaQuarto"})
public class CategoriaQuartoDeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.text.ParseException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        response.getWriter().print("lololol");

        if (request.getParameter("command") != null && request.getParameter("command") != null) {

            switch (request.getParameter("command")) {
                case "categoriaQuarto.listar":
                    if(request.getParameter("hotelId") != null){
                        request.setAttribute("categorias", new CategoriaQuartoDAO().retornaTodasAsCategoriasDoHotel(Integer.parseInt(request.getParameter("hotelId"))));
                        request.setAttribute("hotel", new HotelDAO().retornaHotelComID(Integer.parseInt(request.getParameter("hotelId"))));
                        request.getRequestDispatcher("/CategoriaDeQuartos/index.jsp").forward(request, response);
                    }else{
                        request.getRequestDispatcher("/CategoriaDeQuartos/erro.jsp").forward(request, response);
                    }
                    break;

                case "categoriaQuarto.inserir":

                    if (request.getParameter("hotelId") != null) {

                        if (request.getParameter("action") != null && request.getParameter("action").equals("inserir")) {

                            CategoriaQuarto cat = new CategoriaQuarto();

                            cat.setNome(request.getParameter("nome"));
                            cat.setDescricao(request.getParameter("descricao"));
                            cat.setValor(Double.parseDouble(request.getParameter("valor")));
                            cat.setHotelCodigo(Integer.parseInt(request.getParameter("hotelId")));

                            CategoriaQuartoDAO cD = new CategoriaQuartoDAO();
                            cD.inserir(cat);

                            response.sendRedirect("controller?command=categoriaQuarto.listar&hotelId="+cat.getHotelCodigo());

                        } else {

                            request.getRequestDispatcher("/CategoriaDeQuartos/inserir.jsp").forward(request, response);

                        }

                    } else {
                        request.getRequestDispatcher("/CategoriaDeQuartos/erro.jsp").forward(request, response);
                    }

                    
                break;
                case "categoriaQuarto.remover":  
                {
                    
                    if(request.getParameter("id") != null){
                    
                        CategoriaQuarto q = CategoriaQuartoDAO.retornaCategoriaComID(Integer.parseInt(request.getParameter("id")));

                        CategoriaQuartoDAO cQ = new CategoriaQuartoDAO();
                        cQ.remover(q.getCodigo());

                        response.sendRedirect("controller?command=categoriaQuarto.listar&hotelId="+q.getHotelCodigo());
                    
                    }else{
                        
                        request.getRequestDispatcher("/CategoriaDeQuartos/erro.jsp").forward(request, response);
                        
                    }
                    
                } break;
                
                case "categoriaQuarto.alterar":
                    
                    if (request.getParameter("action") != null && request.getParameter("action").equals("alterar")) {

                        CategoriaQuarto cat = CategoriaQuartoDAO.retornaCategoriaComID(Integer.parseInt(request.getParameter("catId")));

                        cat.setNome(request.getParameter("nome"));
                        cat.setDescricao(request.getParameter("descricao"));
                        cat.setValor(Double.parseDouble(request.getParameter("valor")));
                        cat.setCodigo(Integer.parseInt(request.getParameter("catId")));
                        
                        CategoriaQuartoDAO cD = new CategoriaQuartoDAO();
                        cD.alterar(cat);
                        
                        response.sendRedirect("controller?command=categoriaQuarto.listar&hotelId="+cat.getHotelCodigo());
                        
                        
                    } else {
                        
                        if(request.getParameter("catId") != null){
                        
                            dao.ClienteDAO dao = new ClienteDAO();
                            
                            CategoriaQuartoDAO cD = new CategoriaQuartoDAO();
                            
                            CategoriaQuarto c = cD.retornaCategoriaComID(Integer.parseInt(request.getParameter("catId")));

                            request.setAttribute("categoria", c);

                            request.getRequestDispatcher("/CategoriaDeQuartos/alterar.jsp").forward(request, response);

                        }else{
                            
                            response.getWriter().print("ID invalido");
                            
                        }

                    }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
