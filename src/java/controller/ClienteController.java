/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.ClienteDAO;
import java.io.IOException;
import java.sql.Date;
import java.util.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Cliente.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cliente;
/**
 *
 * @author carlbr
 */
@WebServlet(name = "ClienteServlet", urlPatterns = {"/cliente"})
public class ClienteController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.text.ParseException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        response.getWriter().print("lololol");

        if (request.getParameter("command") != null && request.getParameter("command") != null) {

            switch (request.getParameter("command")) {
                case "cliente.listar":
                    request.setAttribute("clientes", new ClienteDAO().listar());
                    request.getRequestDispatcher("/Cliente/index.jsp").forward(request, response);
                    
                    break;
                    
                case "cliente.inserir":
                    
                    if (request.getParameter("action") != null && request.getParameter("action").equals("inserir")) {

                        model.Cliente cl = new model.Cliente();

                        cl.setNome(request.getParameter("nome"));
                        cl.setEmail(request.getParameter("email"));
                        
                        ClienteDAO clDAO = new ClienteDAO();
                        clDAO.inserir(cl, request.getParameter("senha"));
                        
                        response.sendRedirect("controller?command=cliente.listar");
                        
                        
                    } else {
                        
                        request.setAttribute("umaString", "valor");

                        request.getRequestDispatcher("/Cliente/inserir.jsp").forward(request, response);

                    }
                break;
                case "cliente.remover":  
                {
                    ClienteDAO clDAO = new ClienteDAO();
                
                    clDAO.remover(Integer.parseInt(request.getParameter("id")));
                    
                    response.sendRedirect("controller?command=cliente.listar");
                    
                    
                } break;
                
                case "cliente.alterar":
                    
                    if (request.getParameter("action") != null && request.getParameter("action").equals("alterar")) {

                        Cliente cl = new Cliente(Integer.parseInt(request.getParameter("id")), request.getParameter("nome"), request.getParameter("email"));

                        ClienteDAO clDAO = new ClienteDAO();
                        
                        if(request.getParameter("senha") != null)
                            clDAO.alterar(cl, request.getParameter("senha"));
                        else
                            clDAO.alterar(cl);
                        
                        response.sendRedirect("controller?command=cliente.listar");
                        
                        
                    } else {
                        
                        if(request.getParameter("id") != null){
                        
                            dao.ClienteDAO dao = new ClienteDAO();

                            model.Cliente cl = dao.retornaClienteComID(Integer.parseInt(request.getParameter("id")));

                            request.setAttribute("cliente", cl);

                            request.getRequestDispatcher("/Cliente/alterar.jsp").forward(request, response);

                        }else{
                            
                            response.getWriter().print("ID invalido");
                            
                        }

                    }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
