/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.*;
import model.Quarto;

/**
 *
 * @author carlbr
 */
public class Cliente {
    private Integer codigo;
    private String nome;
    private String email;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    public Cliente () {
        
    }
    
    public Cliente(Integer codigo, String nome, String email){
        
        this.codigo = codigo;
        this.nome = nome;
        this.email = email;
        
    }
    
    public Integer getCodigo() {
        return codigo;
    }
    
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
    
}
