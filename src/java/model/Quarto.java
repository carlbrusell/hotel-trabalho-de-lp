/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author carlbr
 */
public class Quarto {
    private Integer numero;
    private Integer categoriaQuartoId;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getCategoriaQuartoId() {
        return categoriaQuartoId;
    }

    public void setCategoriaQuartoId(Integer categoriaQuartoId) {
        this.categoriaQuartoId = categoriaQuartoId;
    }

}
