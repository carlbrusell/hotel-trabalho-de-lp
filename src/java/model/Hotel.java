/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author carlbrusell
 */
public class Hotel {
    
    private Integer codigo;
    private String nome;
    private String endereco;
    private int hotelAlternativo;
    private ArrayList categoriaQuartos;

    /**
     * @return the codigo
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the endereco
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * @param endereco the endereco to set
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    /**
     * @return the hotelAlternativo
     */
    public int getHotelAlternativo() {
        return hotelAlternativo;
    }

    /**
     * @param hotelAlternativo the hotelAlternativo to set
     */
    public void setHotelAlternativo(int hotelAlternativo) {
        this.hotelAlternativo = hotelAlternativo;
    }

    /**
     * @return the categoriaQuartos
     */
    public ArrayList getCategoriaQuartos() {
        return categoriaQuartos;
    }

    /**
     * @param categoriaQuartos the categoriaQuartos to set
     */
    public void setCategoriaQuartos(ArrayList categoriaQuartos) {
        this.categoriaQuartos = categoriaQuartos;
    }
    
}
