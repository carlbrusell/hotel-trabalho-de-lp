/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.HotelDAO;
import java.util.ArrayList;

/**
 *
 * @author carlbrusell
 */
public class CategoriaQuarto {
    
    private int codigo;
    private String nome;
    private double valor;
    private ArrayList quartos;
    private String descricao;
    private ArrayList servicos;
    private int hotelCodigo;

    public CategoriaQuarto() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public ArrayList getQuartos() {
        return quartos;
    }

    public void setQuartos(ArrayList quartos) {
        this.quartos = quartos;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public ArrayList getServicos() {
        return servicos;
    }

    public void setServicos(ArrayList servicos) {
        this.servicos = servicos;
    }

    public int getHotelCodigo() {
        return hotelCodigo;
    }

    public void setHotelCodigo(int hotelCodigo) {
        this.hotelCodigo = hotelCodigo;
    }
    
    public Hotel getHotel() {
        
        HotelDAO hl = new HotelDAO();
                
        return hl.retornaHotelComID(this.hotelCodigo);
    }
    
    public void setHotel(Hotel h) {
        
        this.hotelCodigo = h.getCodigo();
        
    }
    
    
    
}
