/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author carlbr
 */
class ConexaoSingleton {
    
    public static ConexaoSingleton conexao;
    private Connection conn;
    
    private ConexaoSingleton() throws SQLException, ClassNotFoundException {
    }
    
    public static ConexaoSingleton getInstance() throws SQLException, ClassNotFoundException {
        if(conexao == null)
            conexao = new ConexaoSingleton();
        
        return conexao;
        
    }
    
    public Connection getConnection() throws SQLException, ClassNotFoundException {
        if(conn == null){
            Class.forName("com.mysql.jdbc.Driver");
             conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hotel", "root", "");
        }
        
        return conn;
        
    }
    
    
}