package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cliente;
import model.Hotel;
import dao.HotelDAO;
import model.CategoriaQuarto;
import model.Quarto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author carlbr
 */
public class CategoriaQuartoDAO {

    public void inserir(CategoriaQuarto cat) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            System.out.println("entrou no DAO");
            
            SimpleDateFormat sdf = new SimpleDateFormat();
            
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO categoria_quarto (nome, descricao, valor, codigo_hotel) VALUES (?, ?, ?, ?)");
            
            stmt.setString(1, cat.getNome());
            
            stmt.setString(2, cat.getDescricao());
            
            stmt.setDouble(3, cat.getValor());
            
            stmt.setInt(4, cat.getHotelCodigo());
            
            System.out.println("vamos tentar inserir");
            
            stmt.execute();
            
            System.out.println("tentativa de insercao que deu certo");
        
        }catch(ClassNotFoundException | SQLException e){
            System.out.println("deu ruim");
            e.printStackTrace();
        
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
    }

    public void alterar(CategoriaQuarto cat) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("UPDATE categoria_quarto SET nome = ?, descricao = ?, valor = ?, codigo_hotel = ? WHERE codigo = ?");
            
            stmt.setString(1, cat.getNome());
            
            stmt.setString(2, cat.getDescricao());
            
            stmt.setDouble(3, cat.getValor());
            
            stmt.setInt(4, cat.getHotelCodigo());
            
            stmt.setInt(5, cat.getCodigo());
            
            stmt.execute();
        
        }catch(Exception e){
        
            System.out.println("deu ruim");
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
    }

    public void remover(Integer id) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM categoria_quarto WHERE codigo = ?");
            
            stmt.setInt(1, id);
            
            stmt.execute();
        
        }catch(Exception e){
        
            System.out.println("deu ruim");
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
    public ArrayList retornaTodasAsCategoriasDoHotel(int id){
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM categoria_quarto WHERE codigo_hotel = ?");
            
            stmt.setInt(1, id);
            
            result = stmt.executeQuery();
            
            ArrayList<CategoriaQuarto> lista = new ArrayList();
            
            
            while(result.next()){
                
                CategoriaQuarto cat = new CategoriaQuarto();
                
                cat.setDescricao(result.getString("descricao"));
                
                cat.setNome(result.getString("NOME"));
                
                cat.setCodigo(result.getInt("codigo"));
                
                cat.setValor(result.getDouble("valor"));
                
                cat.setHotelCodigo(result.getInt("codigo_hotel"));
                
                lista.add(cat);
            }
            
            
            return lista;
            
            
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        return null;
    }
    
    
    public static CategoriaQuarto retornaCategoriaComID(int id){
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM categoria_quarto WHERE codigo = ?");
            
            stmt.setInt(1, id);
            
            result = stmt.executeQuery();
            
            CategoriaQuarto cat = new CategoriaQuarto();
            
            cat.setCodigo(id);
            
            while(result.next()){
                cat.setNome(result.getString("NOME"));
                cat.setDescricao(result.getString("descricao"));
                cat.setHotelCodigo(Integer.parseInt(result.getString("codigo_hotel")));
                cat.setValor(Double.parseDouble(result.getString("valor")));
                
                return cat;
            }
            
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        return null;
    }

    
}
