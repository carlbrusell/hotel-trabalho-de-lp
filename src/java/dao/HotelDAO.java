package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cliente;
import model.Hotel;
import dao.CategoriaQuartoDAO;
import model.Quarto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author carlbr
 */
public class HotelDAO implements GenericoDAO<Hotel> {

    public void inserir(Hotel hotel) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            System.out.println("entrou no DAO");
            
            SimpleDateFormat sdf = new SimpleDateFormat();
            
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO HOTEL (NOME, ENDERECO, HOTEL_ALTERNATIVO) VALUES (?, ?, ?)");
            
            stmt.setString(1, hotel.getNome());
            
            stmt.setString(2, hotel.getEndereco());
            
            stmt.setInt(3, hotel.getHotelAlternativo());
            
            System.out.println("vamos tentar inserir");
            
            stmt.execute();
            
            System.out.println("tentativa de insercao que deu certo");
        
        }catch(ClassNotFoundException | SQLException e){
            
            System.out.println("deu ruim");
            e.printStackTrace();
        
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
        
    }

    @Override
    public void alterar(Hotel hotel) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("UPDATE hotel SET nome = ?, endereco = ?, hotel_alternativo = ? WHERE codigo = ?");
            
            stmt.setString(1, hotel.getNome());
            
            stmt.setString(2, hotel.getEndereco());
            
            stmt.setInt(3, hotel.getHotelAlternativo());
            
            stmt.setInt(4, hotel.getCodigo());
            
            stmt.execute();
            
            connection.close();
        
        }catch(Exception e){
        
            System.out.println("deu ruim");
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
    }

    public void remover(Integer id) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM hotel WHERE codigo = ?");
            
            stmt.setInt(1, id);
            
            stmt.execute();
            
            connection.close();
        
        }catch(Exception e){
        
            System.out.println("deu ruim");
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
    }

    @Override
    public List listar() {
        
        List<Hotel> hoteis = new ArrayList<Hotel>();
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            connection = ConexaoSingleton.getInstance().getConnection();

            Statement stmt = connection.createStatement();
            
            result = stmt.executeQuery("SELECT * FROM hotel");
            
            CategoriaQuartoDAO cat = new CategoriaQuartoDAO();
            
            while(result.next()){
                
                Hotel hotel = new Hotel();
                hotel.setCodigo(result.getInt("codigo"));
                hotel.setNome(result.getString("nome"));
                hotel.setEndereco(result.getString("endereco"));
                hotel.setHotelAlternativo(result.getInt("hotel_alternativo"));
                hotel.setCategoriaQuartos(cat.retornaTodasAsCategoriasDoHotel(hotel.getCodigo()));
                
                hoteis.add(hotel);
                
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
        return hoteis;
        
    }
    
    public Hotel retornaHotelComID(int id){
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM hotel WHERE codigo = ?");
            
            stmt.setInt(1, id);
            
            result = stmt.executeQuery();
            
            Hotel h = new Hotel();
            
            h.setCodigo(id);
            
            CategoriaQuartoDAO cat = new CategoriaQuartoDAO();
            
            while(result.next()){
                h.setNome(result.getString("nome"));
                h.setEndereco(result.getString("endereco"));
                h.setHotelAlternativo(result.getInt("hotel_alternativo"));
                h.setCategoriaQuartos(cat.retornaTodasAsCategoriasDoHotel(h.getCodigo()));
                
                return h;
            }
            
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        return null;
    }

    @Override
    public void remover(Hotel e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
