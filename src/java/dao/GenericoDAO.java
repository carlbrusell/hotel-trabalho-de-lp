/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;

/**
 *
 * @author carlbr
 */
public interface GenericoDAO<E> {
    public void inserir(E e);
    public void alterar(E e);
    public void remover(E e);
    public List<E> listar();
}