package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cliente;
import model.Quarto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author carlbr
 */
public class ClienteDAO implements GenericoDAO<Cliente> {

    public void inserir(Cliente cliente, String senha) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
             System.out.println("entrou no DAO");
            
            SimpleDateFormat sdf = new SimpleDateFormat();
            
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO usuario (nome, email, senha, tipo_usuario) VALUES (?, ?, ?, ?)");
            
            stmt.setString(1, cliente.getNome());
            
            stmt.setString(2, cliente.getEmail());
            
            stmt.setString(3, senha);
            
            stmt.setString(4, "cliente");
            
             System.out.println("vamos tentar inserir");
            
            stmt.execute();
            
            System.out.println("tentativa de insercao que deu certo");
        
        }catch(ClassNotFoundException | SQLException e){
            System.out.println("deu ruim");
            e.printStackTrace();
        
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
    }

    public void alterar(Cliente cliente, String senha) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("UPDATE usuario SET nome = ?, email = ?, senha = ? WHERE codigo = ?");
            
            stmt.setString(1, cliente.getNome());
            
            stmt.setString(2, cliente.getEmail());
            
            stmt.setString(3, senha);
            
            stmt.setInt(4, cliente.getCodigo());
            
            stmt.execute();
        
        }catch(Exception e){
        
            System.out.println("deu ruim");
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
    }

    public void remover(Integer id) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM usuario WHERE codigo = ?");
            
            stmt.setInt(1, id);
            
            stmt.execute();
        
        }catch(Exception e){
        
            System.out.println("deu ruim");
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
    }
    
    public Cliente retornaClienteComID(int id){
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM usuario WHERE codigo = ?");
            
            stmt.setInt(1, id);
            
            result = stmt.executeQuery();
            
            Cliente cl = new Cliente();
            
            cl.setCodigo(id);
            
            while(result.next()){
                cl.setNome(result.getString("NOME"));
                cl.setEmail(result.getString("email"));
                
                return cl;
            }
            
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        return null;
    }

    @Override
    public List listar() {
        
        List<Cliente> clientes = new ArrayList<Cliente>();
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            connection = ConexaoSingleton.getInstance().getConnection();

            Statement stmt = connection.createStatement();
            
            result = stmt.executeQuery("SELECT * FROM usuario");
            
            while(result.next()){
                
                Cliente cliente = new Cliente(result.getInt("codigo"), result.getString("NOME"), result.getString("email"));
                
                clientes.add(cliente);
                
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
        return clientes;
        
    }

    @Override
    public void remover(Cliente e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void inserir(Cliente e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void alterar(Cliente cliente) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("UPDATE usuario SET nome = ?, email = ? WHERE codigo = ?");
            
            stmt.setString(1, cliente.getNome());
            
            stmt.setString(2, cliente.getEmail());
            
            stmt.setInt(3, cliente.getCodigo());
            
            stmt.execute();
        
        }catch(Exception e){
        
            System.out.println("deu ruim");
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
    }
    
}
