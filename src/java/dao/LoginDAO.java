/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cliente;

/**
 *
 * @author carlbrusell
 */
public class LoginDAO {
    
    public Cliente login(String email, String senha){
        
        Connection connection = null;
        
        ResultSet result = null;
        
        try {
            connection = ConexaoSingleton.getInstance().getConnection();
            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM usuario WHERE email = ? AND senha = ?");
            
            stmt.setString(1, email);
            
            stmt.setString(2, senha);
            
            result = stmt.executeQuery();
            
            Cliente cl = new Cliente();
            
            while(result.next()){
                cl.setNome(result.getString("nome"));
                cl.setEmail(result.getString("email"));
                
                return cl;
            }
            
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        return null;
    }
    
}
