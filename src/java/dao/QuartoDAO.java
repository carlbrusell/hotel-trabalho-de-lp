/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.CategoriaQuarto;
import model.Cliente;
import model.Quarto;

/**
 *
 * @author carlbr
 */
public class QuartoDAO {
    
    public List<Quarto> listarPorCategoriaQuarto(CategoriaQuarto cat) {
        
        Connection connection = null;
        
        ResultSet result = null;
        
        ArrayList<Quarto> quartos = new ArrayList();
        
        try {
            
            connection = ConexaoSingleton.getInstance().getConnection();

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM quarto WHERE categoria_id = ?");
            
            stmt.setInt(1, cat.getCodigo());
            
            result = stmt.executeQuery();
            
            while(result.next()){
                
                Quarto quarto = new Quarto();
                quarto.setCategoriaQuartoId(result.getInt("categoria_id"));
                quarto.setNumero(result.getInt("numero"));
                quartos.add(quarto);
                
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            
            try {
                
                if(result != null && !result.isClosed()){
                    result.close();
                }
                
                if(connection != null && connection.isClosed()){
                    connection.close();
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
        return quartos;
        
        
    }
    
}
