<%-- 
    Document   : inserir
    Created on : Nov 12, 2014, 2:15:52 PM
    Author     : carlbrusell
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="model.Hotel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Inserir Hotel</h1>
        
        <form action="" method="post">
            
            <input type="hidden" name="action" value="inserir">
            
            <label for="nome">Nome:</label>
            
            <input type="text" name="nome">
            
            <br /><br />
            
            <label for="endereco">Endereço:</label>
            
            <input type="text" name="endereco">
            
            <br /><br />
            
            <label for="nome">Hotel Alternativo:</label>
            <select name="hotelAlternativo">
                <%
            
                ArrayList<Hotel> lista = new ArrayList();
        
                lista = (ArrayList) request.getAttribute("hoteis");
            
            
                for(Hotel h : lista){
                    out.println("<option value=\""+h.getCodigo()+"\">"+h.getNome()+"</option>");
                }
                %>
            </select>
            <br /><br />
            
            
            <input type="submit" value="OK">
        </form>
        
    </body>
</html>
