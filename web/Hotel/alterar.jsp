<%-- 
    Document   : alterar
    Created on : Oct 15, 2014, 5:23:08 PM
    Author     : carlbrusell
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="model.Hotel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Alterar Hotel</h1>
        
        <%
            
            Hotel hotel = (Hotel) request.getAttribute("hotel");
            
            ArrayList<Hotel> lista = new ArrayList();
        
            lista = (ArrayList) request.getAttribute("hoteis");
         
            %>
        
        <form action="" method="post">
            <input type="hidden" name="action" value="alterar">
            <input type="hidden" name="codigo" value="<% out.println(hotel.getCodigo().toString());%>">
            <div id="linha">
                <label for="nome">Nome:</label>
                <input type="text" value='<% out.println(hotel.getNome()); %>' name="nome">
            </div>
            <div id="linha">
                <label for="endereco">Endereço:</label>
                <input type="text" value='<% out.println(hotel.getEndereco()); %>' name="endereco">
            </div>
            <div id="linha">
                <label for="nascimento">Hotel Alternativo:</label>
                <select name="hotelAlternativo">
                <%
                for(Hotel h : lista){
                    
                    if(hotel.getCodigo() == h.getCodigo()) continue;
                    
                    if(hotel.getHotelAlternativo() == h.getCodigo())
                    
                        out.println("<option selected value=\""+h.getCodigo()+"\">"+h.getNome()+"</option>");
                    
                    else 
                        
                        out.println("<option value=\""+h.getCodigo()+"\">"+h.getNome()+"</option>");
                }
                %>
                </select>
            </div>
            <div id="linha">
                <label for="submit">
                    <input type="submit" name="submit" value="OK">
                </label>
            </div>
        </form>
    </body>
</html>
