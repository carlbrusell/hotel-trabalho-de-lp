<%-- 
    Document   : listar
    Created on : Nov 14, 2014, 6:12:40 PM
    Author     : carlbrusell
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="model.Hotel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hotéis da Rede Accor</h1>
        
        <a href="?command=hotel.inserir">Inserir Hotel</a><br />&nbsp;
        
        <%
            
        ArrayList<Hotel> lista = new ArrayList();
        
        lista = (ArrayList) request.getAttribute("hoteis");
        
        out.println("<table width=\"50%\">");
        
        for(Hotel h : lista){
            
            out.println("<tr><td width=\"50%\">");
            
            out.println(h.getNome());
            
            out.println("</td>");
            
            out.println("<td>"
                    + "<a href=\"?command=hotel.alterar&id="+h.getCodigo()+"\">Editar</a>"
                    + "</td><td>"
                    + "<a href=\"?command=categoriaQuarto.listar&hotelId="+h.getCodigo()+"\">Categorias</a>"
                    + "</td><td>"
                    + "<a onclick=\"return confirm('Tem certeza que deseja excluir este hotel?');\" href=\"?command=hotel.remover&id="+h.getCodigo()+"\">Deletar</a>"
                    + "</td></tr>");
            
            
        }
            
        out.println("</table>");
        
        %>
        
    </body>
</html>
