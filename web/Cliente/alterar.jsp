<%-- 
    Document   : alterar
    Created on : Oct 15, 2014, 5:23:08 PM
    Author     : carlbrusell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Alterar Cliente</h1>
        
        <%
            
            model.Cliente cliente = (model.Cliente) request.getAttribute("cliente");
         
            %>
        
        <form action="" method="post">
            <input type="hidden" name="action" value="alterar">
            <input type="hidden" name="id" value="<% out.println(cliente.getCodigo().toString()); %>">
            <div id="linha">
                <label for="nome">Nome:</label>
                <input type="text" value='<% out.println(cliente.getNome()); %>' name="nome">
            </div>
            <div id="linha">
                <label for="cpf">Email:</label>
                <input type="text" value='<% out.println(cliente.getEmail()); %>' name="email">
            </div>
            <div id="linha">
                <label for="nascimento">Senha:</label>
                <input type="password" value='' name="senha">
            </div>
            <div id="linha">
                <label for="submit">
                    <input type="submit" name="submit" value="OK">
                </label>
            </div>
        </form>
    </body>
</html>
