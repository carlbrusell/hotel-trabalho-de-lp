<%-- 
    Document   : inserir
    Created on : Oct 15, 2014, 5:23:08 PM
    Author     : carlbrusell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Cadastro de novo cliente</h1>
        
        Bem vindo a rede de Hoteis Accor! Registre-se já e comece aproveitar
        as vantagens de hospedar em um de nossos hoteis.
        <br />&nbsp;
        <form action="" method="post">
            <input type="hidden" name="action" value="inserir">
            <div id="linha">
                <label for="nome">Nome:</label>
                <input type="text" name="nome">
            </div>
            <div id="linha">
                <label for="cpf">CPF:</label>
                <input type="text" name="cpf">
            </div>
            <div id="linha">
                <label for="nascimento">Data de Nascimento:</label>
                <input type="text" placeholder="DD/MM/AAAA" name="nascimento">
            </div>
            <div id="linha">
                <label for="submit">
                    <input type="submit" name="submit" value="OK">
                </label>
            </div>
        </form>
    </body>
</html>
