<%-- 
    Document   : index
    Created on : Sep 25, 2014, 8:56:13 AM
    Author     : carlbr
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="model.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Listagem de Clientes</h1>
        
        <br />
        
        <a href="${pageContext.request.contextPath}/controller?command=cliente.inserir">inserir clientes</a><br /><br />
        
        <%
            
        ArrayList<Cliente> lista = new ArrayList();
        
        lista = (ArrayList) request.getAttribute("clientes");
        
        out.println("<table width=\"50%\">");
        
        for(Cliente c : lista){
            
            out.println("<tr><td width=\"50%\">");
            
            out.println(c.getNome());
            
            out.println("</td>");
            
            out.println("<td>"
                    + "<a href=\"?command=cliente.alterar&id="+c.getCodigo()+"\">Editar</a>"
                    + "</td><td>"
                    + "<a onclick=\"return confirm('Tem certeza que deseja excluir este cliente?');\" href=\"?command=cliente.remover&id="+c.getCodigo()+"\">Deletar</a>"
                    + "</td></tr>");
            
            
        }
            
        out.println("</table>");
        
        %>
    </body>
</html>
