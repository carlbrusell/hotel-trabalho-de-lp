<%-- 
    Document   : inserir
    Created on : Oct 16, 2014, 8:06:26 AM
    Author     : carlbrusell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Inserir novo Endereço</h1>
        
        
        <form action="" method="post">
            <input type="hidden" name="action" value="inserir">
            <div id="linha">
                <label for="logradouro">Logradouro:</label>
                <input type="text" name="logradouro">
            </div>
            <div id="linha">
                <label for="nome">Nome:</label>
                <input type="text" name="nome">
            </div>
            <div id="linha">
                <label for="bairro">Bairro:</label>
                <input type="text" placeholder="DD/MM/AAAA" name="nascimento">
            </div>
            <div id="linha">
                <label for="endereco">Complemento:</label>
                <input type="text" name="complemento">
            </div>
            <div id="linha">
                <label for="endereco">CEP:</label>
                <input type="text" name="cep">
            </div>
            <div id="linha">
                <label for="submit">
                    <input type="submit" name="submit" value="OK">
                </label>
            </div>
        </form>
        
        
    </body>
</html>
