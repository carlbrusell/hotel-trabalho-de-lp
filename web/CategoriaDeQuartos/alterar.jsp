<%-- 
    Document   : alterar
    Created on : Oct 15, 2014, 5:23:08 PM
    Author     : carlbrusell
--%>

<%@page import="model.CategoriaQuarto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Hotel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Alterar Hotel</h1>
        
        <%
            
            CategoriaQuarto categoria = (CategoriaQuarto) request.getAttribute("categoria");
           
            %>
        
            <form action="" method="post">
            
            <input type="hidden" name="action" value="alterar">
            <input type="hidden" name="catId" value="<%=request.getParameter("catId")%>">
            
            <label for="nome">Nome:</label>
            
            <input type="text" name="nome" value='<%=categoria.getNome()%>'>
            
            <br /><br />
            
            <label for="endereco">Descrição</label>
            
            <input type="text" name="descricao" value='<%=categoria.getDescricao()%>'>
            
            <br /><br />
            
            <label for="nome">Valor</label>
            
            <input type="text" name="valor" value='<%=categoria.getValor()%>'>
            
            <br /><br />
            
            
            <input type="submit" value="OK">
        </form>
    </body>
</html>
