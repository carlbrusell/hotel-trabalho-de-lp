<%-- 
    Document   : listar
    Created on : Nov 14, 2014, 6:12:40 PM
    Author     : carlbrusell
--%>

<%@page import="model.Hotel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.CategoriaQuarto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%
            
        ArrayList<CategoriaQuarto> lista = new ArrayList();
        
        Hotel hotel = new Hotel();
        
        lista = (ArrayList) request.getAttribute("categorias");
        
        hotel = (Hotel) request.getAttribute("hotel");
        
        %>
        <h1>Categorias disponíveis no Hotel <%=hotel.getNome()%></h1>
        
        <a href="?command=categoriaQuarto.inserir&hotelId=<%=hotel.getCodigo()%>">Inserir nova Categoria</a><br />&nbsp;
        
        <%
        
        out.println("<table border=\"1\" width=\"50%\">"
                + "<tr><th>Nome da Categoria</th><th>Valor</th><th>Ações</th></tr>");
        
        for(CategoriaQuarto c : lista){
            
            out.println("<tr><td width=\"50%\">");
            
            out.println(c.getNome());
            
            out.println("</td>");
            
            out.println("<td>"
                    + c.getValor()
                    + "</td><td>"
                    + "<a href=\"?command=categoriaQuarto.alterar&catId="+c.getCodigo()+"\">Editar</a> / "
                    + "<a onclick=\"return confirm('Tem certeza que deseja excluir esta categoria?');\" href=\"?command=categoriaQuarto.remover&id="+c.getCodigo()+"\">Deletar</a>"
                    + "</td></tr>");
            
            
        }
            
        out.println("</table>");
        
        %>
        
    </body>
</html>
