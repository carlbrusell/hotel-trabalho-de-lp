-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 08, 2016 at 01:41 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `categoria_quarto`
--

CREATE TABLE `categoria_quarto` (
  `codigo` int(11) NOT NULL,
  `nome` varchar(300) NOT NULL,
  `descricao` varchar(300) NOT NULL,
  `valor` float NOT NULL,
  `codigo_hotel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categoria_quarto`
--

INSERT INTO `categoria_quarto` (`codigo`, `nome`, `descricao`, `valor`, `codigo_hotel`) VALUES
(1, 'teste', 'teste', 2500, 3),
(2, 'teste', 'teste', 2500, 2),
(3, 'abc', 'caro', 1000000, 6);

-- --------------------------------------------------------

--
-- Table structure for table `categoria_quarto_rel`
--

CREATE TABLE `categoria_quarto_rel` (
  `id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `servico_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categoria_quarto_servicos`
--

CREATE TABLE `categoria_quarto_servicos` (
  `codigo` int(11) NOT NULL,
  `nome` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `codigo` int(11) NOT NULL,
  `nome` varchar(200) CHARACTER SET latin1 NOT NULL,
  `endereco` varchar(200) CHARACTER SET latin1 NOT NULL,
  `hotel_alternativo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`codigo`, `nome`, `endereco`, `hotel_alternativo`) VALUES
(2, 'Novotel Sao Paulo Jaragua Conventions', 'R. Martins Fontes 71 CENTRO', 0),
(3, 'Mercure Sao Paulo Paulista Hotel', 'Rua Sao Carlos Do Pinhal,87 Bela Vista', 5),
(4, 'ibis budget Sao Paulo Sao Joao', 'Av. SÃ£o JoÃ£o, 1.140 01036-100 SAO PAULO', 0),
(5, 'Mercure Sao Paulo Central Towers Hotel', 'Rua Maestro Cardim 407 1323000 SAO PAULO', 0),
(6, 'ibis Sao Paulo Paulista', 'Avenida Paulista 2355 01311300 SAO PAULO', 7),
(7, 'ibis budget Sao Paulo Paulista', 'Rua da Consolacao 2303 1301100 SAO PAULO', 0),
(8, 'Outro hotel', 'endereco', 0),
(9, 'Outro hotel 1', 'endereco', 0),
(10, 'Outro hotel 2', 'endereco', 0),
(11, 'Outro hotel 3', 'endereco', 0),
(12, 'Outro hotel 4', 'endereco', 0),
(13, 'Outro hotel 5', 'endereco', 0);

-- --------------------------------------------------------

--
-- Table structure for table `quarto`
--

CREATE TABLE `quarto` (
  `numero` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `codigo` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(12) NOT NULL,
  `tipo_usuario` enum('funcionario','cliente') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`codigo`, `nome`, `email`, `senha`, `tipo_usuario`) VALUES
(2, 'Eduardo Tello', 'dutello@gmail.com', '', 'cliente');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria_quarto`
--
ALTER TABLE `categoria_quarto`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `categoria_quarto_rel`
--
ALTER TABLE `categoria_quarto_rel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categoria_quarto_servicos`
--
ALTER TABLE `categoria_quarto_servicos`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `quarto`
--
ALTER TABLE `quarto`
  ADD PRIMARY KEY (`numero`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`codigo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria_quarto`
--
ALTER TABLE `categoria_quarto`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `categoria_quarto_rel`
--
ALTER TABLE `categoria_quarto_rel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categoria_quarto_servicos`
--
ALTER TABLE `categoria_quarto_servicos`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
